package pl.sdaacademy.zdjavapol123.vod;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface SeriesRepository extends JpaRepository<Series, Integer> {

    List<Series> findAllByNumberOfEpisodesGreaterThanEqual(int numberOfEpisodes);

    List<Series> findAllBySeasonNumber(int seasonNumber);

    List<Series> findAllByReleaseDateAfter(LocalDate releaseDate);
}
