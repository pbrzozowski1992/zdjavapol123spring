package pl.sdaacademy.zdjavapol123;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Zdjavapol123Application {

	public static void main(String[] args) {
		SpringApplication.run(Zdjavapol123Application.class, args);
	}

}
