package pl.sdaacademy.zdjavapol123.design_patterns.prototype;

public class Student implements Cloneable {

    private String name;
    private String lastName;

    private int age;

    private Course course;

    public Student(String name, String lastName, int age, Course course) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.course = course;
    }


    @Override
    protected Object clone() throws CloneNotSupportedException {
        Student studentClone = (Student) super.clone();
        studentClone.course = (Course) course.clone();
        return studentClone;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", course=" + course +
                '}';
    }

    public static void main(String[] args) {
        Student student = new Student("Jan", "Nowak", 30, new Course("Biology"));
        try {
            Student studentClone = (Student) student.clone();
            System.out.println("Student: " + student);
            System.out.println("Studnet clone: " + studentClone);
            studentClone.name = "Wacław";
            studentClone.course.subject = "History";
            System.out.println("############## after change");
            System.out.println("Student: " + student);
            System.out.println("Studnet clone: " + studentClone);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}

class Course implements Cloneable {
    String subject;

    public Course(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "Course{" +
                "subject='" + subject + '\'' +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
