package pl.sdaacademy.zdjavapol123.design_patterns.prototype;

public class Movie implements Cloneable {

    private String title;
    private int yearOfRelease;

    private Director director;

    public Movie(String title, int yearOfRelease, Director director) {
        this.title = title;
        this.yearOfRelease = yearOfRelease;
        this.director = director;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", yearOfRelease=" + yearOfRelease +
                ", director=" + director +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Movie movieClone = (Movie) super.clone();
        movieClone.director = (Director) director.clone();
        return movieClone;
    }

    public static void main(String[] args) {
        Movie movie = new Movie(
                "Star Wars",
                2015,
                new Director("J.J", "Abrams", TypeOfMovie.ACTION)
        );

        try {
            Movie movieToClone = (Movie) movie.clone();
            System.out.println("Movie: " + movie);
            System.out.println("Movie clone: " + movieToClone);
            movieToClone.director.name = "James";
            System.out.println("AFTER UPDATE");
            System.out.println("Movie: " + movie);
            System.out.println("Movie clone: " + movieToClone);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}

class Director implements Cloneable {
    String name;
    String lastName;
    TypeOfMovie typeOfMovie;

    public Director(String name, String lastName, TypeOfMovie typeOfMovie) {
        this.name = name;
        this.lastName = lastName;
        this.typeOfMovie = typeOfMovie;
    }

    @Override
    public String toString() {
        return "Director{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", typeOfMovie=" + typeOfMovie +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

enum TypeOfMovie {
    ACTION, DRAMA, COMEDY
}