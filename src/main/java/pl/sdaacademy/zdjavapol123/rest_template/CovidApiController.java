package pl.sdaacademy.zdjavapol123.rest_template;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@RequestMapping("/covid")
@RestController
public class CovidApiController {

    private final RestTemplate restTemplate;
    private final String baseUrl;

    @Autowired
    public CovidApiController(@Value("${covid.api.url}") String baseUrl, @CovidHttpClient RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.baseUrl = baseUrl;
    }

    @GetMapping
    public List<CovidStats> getCovidStats() {
        String url = baseUrl+"us/daily.json";
        CovidStats[] covidStats = restTemplate.getForObject(url, CovidStats[].class);
        return Arrays.asList(covidStats);
    }

    @GetMapping("/min_positive")
    public CovidStats getMinPositive() {
        String url = baseUrl+"us/daily.json";
        CovidStats[] covidStats = restTemplate.getForObject(url, CovidStats[].class);
        return Arrays.asList(covidStats).stream().min((first, second) ->{
            return Long.compare(first.getPositive(), second.getPositive());
        }).orElse(null);
    }

    @GetMapping("/average")
    public Double getAverage() {
        String url = baseUrl+"us/daily.json";
        CovidStats[] covidStats = restTemplate.getForObject(url, CovidStats[].class);
        return Arrays.asList(covidStats).stream().mapToLong(cs -> cs.getPositive())
                .average().orElse(-1.0);
    }

}
