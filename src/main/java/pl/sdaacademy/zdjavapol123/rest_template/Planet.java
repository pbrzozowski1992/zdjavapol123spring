package pl.sdaacademy.zdjavapol123.rest_template;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Planet {
    private String name;

    @JsonProperty("surface_water")
    private String surfaceWater;

    private List<String> films;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurfaceWater() {
        return surfaceWater;
    }

    public void setSurfaceWater(String surfaceWater) {
        this.surfaceWater = surfaceWater;
    }

    public List<String> getFilms() {
        return films;
    }

    public void setFilms(List<String> films) {
        this.films = films;
    }
}
