package pl.sdaacademy.zdjavapol123.rest_template;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Person {

    private String name;
    @JsonProperty("hair_color")
    private String hairColor;
    private List<String> films;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public List<String> getFilms() {
        return films;
    }

    public void setFilms(List<String> films) {
        this.films = films;
    }
}
