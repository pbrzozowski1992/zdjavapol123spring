package pl.sdaacademy.zdjavapol123.rest_template;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Configuration
class NetworkConfiguration {

    @Bean
    //@Primary
    @StarWarsHttpClient
    RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    @CovidHttpClient
    RestTemplate getRestTemplate1() {
        return new RestTemplate();
    }
}

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
@interface StarWarsHttpClient {

}

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
@interface CovidHttpClient {

}
