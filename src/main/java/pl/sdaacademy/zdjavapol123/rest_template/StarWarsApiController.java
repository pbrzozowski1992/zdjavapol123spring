package pl.sdaacademy.zdjavapol123.rest_template;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/swapi")
@RestController
public class StarWarsApiController {

    private final RestTemplate restTemplate;

    private final String baseUrl;

    @Autowired
    public StarWarsApiController(@StarWarsHttpClient RestTemplate restTemplate,
                                 @Value("${starwars.api.url}") String baseUrl) {
        this.restTemplate = restTemplate;
        this.baseUrl = baseUrl;
    }

    //    @GetMapping
//    public String fetchStarWarsPerson() {
//        RestTemplate restTemplate = new RestTemplate();
//        String url = "https://swapi.dev/api/people/1?format=json";
//        ResponseEntity<String> result = restTemplate.getForEntity(url, String.class);
//        return result.getBody();
//    }

    @GetMapping("/people")
    public Person fetchStarWarsPerson() {
        String url = baseUrl+"people/1?format=json";
        Person person = restTemplate.getForObject(url, Person.class);
        return person;
    }

    @GetMapping("/planet")
    public Planet fetchStarWarsPlanet() {
        String url = baseUrl+"planets/1?format=json";
        Planet planet = restTemplate.getForObject(url, Planet.class);
        return planet;
    }

    @GetMapping("/planets")
    public List<Planet> fetchStarWarsPlanets() {
        List<Planet> planets = new ArrayList<>();
        String url = baseUrl+"planets?format=json";
        PlanetResults planetResults = new PlanetResults();
        planetResults.setNext(url);
        do {
            planetResults = restTemplate.getForObject(planetResults.getNext(), PlanetResults.class);
            planets.addAll(planetResults.getResults());
        }
        while (planetResults.getNext() != null);
        return planets;
    }

}
