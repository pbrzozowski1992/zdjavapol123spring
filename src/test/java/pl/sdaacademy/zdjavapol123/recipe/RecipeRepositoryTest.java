package pl.sdaacademy.zdjavapol123.recipe;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class RecipeRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;
    @Autowired
    private RecipeRepository recipeRepository;

    @Test
    void given_db_is_empty_when_execute_find_all_then_return_empty_list() {
        //given

        //when
        List<Recipe> results = recipeRepository.findAll();

        //then
        assertTrue(results.isEmpty());
    }

    @Test
    void given_db_contains_record_when_execute_find_all_then_return_all_records_from_db() {
        //given
        testEntityManager.persist(
                new Recipe(
                        "name",
                        "description",
                        100, 4,
                        null,
                        Complexity.EASY)
        );
        testEntityManager.persist(
                new Recipe("name1",
                        "description1",
                        120,
                        3,
                        null,
                        Complexity.HARD)
        );

        //niepoprawnie -> testujemy metodę findAll, a nie metodę saveAll i findAll
//        recipeRepository.saveAll(Arrays.asList(
//                new Recipe("name", "description", 100, 4, null, Complexity.EASY),
//                new Recipe("name1", "description1", 120, 3, null, Complexity.HARD)
//        ));

        //when
        List<Recipe> results = recipeRepository.findAll();

        //then
        assertEquals(2, results.size());
    }


    @Test
    void given_db_with_record_with_provided_id_when_execute_find_by_id_then_return_record_from_db() {
        //given
        Recipe recipe = testEntityManager.persist(new Recipe(
                "name",
                "description",
                100, 4,
                null,
                Complexity.EASY)
        );

        //when
        Recipe result = recipeRepository.findById(recipe.getId()).get();

        //then
        assertEquals(recipe, result);
    }

    @Test
    void given_db_without_record_with_provided_id_when_execute_find_by_id_then_return_empty_optional() {
        //given
        long id = 1;

        //when
        Optional<Recipe> recipe = recipeRepository.findById(id);

        //then
        assertTrue(recipe.isEmpty());
    }

    /*
     List<Recipe> findAllByIngredientsContains(String ingredients, Pageable pageable);

    List<Recipe> findAllByComplexity(Complexity complexity, Pageable pageable);
     */

    @Test
    void given_db_with_records_with_provided_ingredients_when_execute_find_all_by_ingredients_contains_then_return_records_from_db() {
        //given
        testEntityManager.persist(
                new Recipe(
                        "name",
                        "description",
                        100, 4,
                        "milk",
                        Complexity.EASY)
        );
        testEntityManager.persist(
                new Recipe("name1",
                        "description1",
                        120,
                        3,
                        "chicken",
                        Complexity.HARD)
        );

        //when
        List<Recipe> results = recipeRepository.findAllByIngredientsContains("milk", null);

        //then
        assertEquals(1, results.size());
        assertEquals("name", results.get(0).getName());
    }

    @Test
    void given_db_without_records_with_provided_ingredients_when_execute_find_all_by_ingredients_contains_then_return_empty_list() {
        //given
        testEntityManager.persist(
                new Recipe(
                        "name",
                        "description",
                        100, 4,
                        "milk",
                        Complexity.EASY)
        );
        testEntityManager.persist(
                new Recipe("name1",
                        "description1",
                        120,
                        3,
                        "chicken",
                        Complexity.HARD)
        );

        //when
        List<Recipe> results = recipeRepository.findAllByIngredientsContains("beef", null);

        //then
        assertTrue(results.isEmpty());
    }

    @Test
    void given_db_with_records_with_provided_complexity_when_execute_find_all_by_complexity_then_return_records_from_db() {
        //given
        testEntityManager.persist(
                new Recipe(
                        "name",
                        "description",
                        100, 4,
                        "milk",
                        Complexity.EASY)
        );
        testEntityManager.persist(
                new Recipe(
                        "name2",
                        "description2",
                        100, 4,
                        "milk",
                        Complexity.EASY)
        );
        testEntityManager.persist(
                new Recipe("name1",
                        "description1",
                        120,
                        3,
                        "chicken",
                        Complexity.HARD)
        );

        //when
        List<Recipe> results = recipeRepository.findAllByComplexity(Complexity.EASY, null);

        //then
        assertEquals(2, results.size());
        assertEquals("name", results.get(0).getName());
        assertEquals("name2", results.get(1).getName());
    }

    @Test
    void given_db_without_records_with_provided_complexity_when_execute_find_all_by_complexity_then_return_empty_list() {
        //given
        testEntityManager.persist(
                new Recipe(
                        "name",
                        "description",
                        100, 4,
                        "milk",
                        Complexity.EASY)
        );
        testEntityManager.persist(
                new Recipe("name1",
                        "description1",
                        120,
                        3,
                        "chicken",
                        Complexity.EASY)
        );

        //when
        List<Recipe> results = recipeRepository.findAllByComplexity(Complexity.HARD, null);

        //then
        assertTrue(results.isEmpty());
    }
}