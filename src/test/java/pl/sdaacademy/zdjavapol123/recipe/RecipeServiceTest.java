package pl.sdaacademy.zdjavapol123.recipe;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class RecipeServiceTest {

    @Test
    void given_repo_with_items_when_get_recipe_by_id_then_return_recipe_from_repo() {
        //given
        RecipeRepository recipeRepository = Mockito.mock(RecipeRepository.class);
        RecipeService recipeService = new RecipeService(recipeRepository);
        Recipe expectedResult = new Recipe();
        Mockito.when(recipeRepository.findById(1L)).thenReturn(Optional.of(expectedResult));

        //when
        Recipe result = recipeService.getRecipeById(1L);

        //then
        assertEquals(expectedResult, result);
    }

    @Test
    void given_repo_with_items_when_get_recipe_by_not_existing_id_then_throw_exception(){
        //given
        RecipeRepository recipeRepository = Mockito.mock(RecipeRepository.class);
        RecipeService recipeService = new RecipeService(recipeRepository);
        Mockito.when(recipeRepository.findById(1L)).thenReturn(Optional.empty());

        //when
        //then
        assertThrows(NoRecipeFoundException.class,()-> recipeService.getRecipeById(1L));
//        try {
//            recipeService.getRecipeById(1L);
//        }catch (RuntimeException e) {
//            assertTrue(e instanceof NoRecipeFoundException);
//        }
    }

    @Test
    void given_repo_without_recipe_to_delete_when_delete_recipe_by_not_existing_id_then_throw_exception() {
        //given
        RecipeRepository recipeRepository = Mockito.mock(RecipeRepository.class);
        RecipeService recipeService = new RecipeService(recipeRepository);
        Mockito.when(recipeRepository.findById(1L)).thenReturn(Optional.empty());

        //when
        //then
        assertThrows(NoRecipeFoundException.class, ()->recipeService.deleteRecipe(1L));
//        try {
//            recipeService.deleteRecipe(1L);
//        } catch (RuntimeException e) {
//            assertTrue(e instanceof NoRecipeFoundException);
//        }
    }

    @Test
    void given_repo_with_recipe_to_delete_when_delete_recipe_by_existing_id_then_return_deleted_element() {
        //given
        Recipe expectedResult = Mockito.mock(Recipe.class);
        RecipeRepository recipeRepository = Mockito.mock(RecipeRepository.class);
        RecipeService recipeService = new RecipeService(recipeRepository);
        Mockito.when(recipeRepository.findById(1L)).thenReturn(Optional.of(expectedResult));

        //when
        Recipe result = recipeService.deleteRecipe(1L);

        //then
        assertEquals(expectedResult, result);
    }

    @Test
    void given_repo_with_not_existing_recipe_when_add_new_recipe_with_unique_name_then_recipe_should_be_added() {
        //given
        Recipe expectedResult = Mockito.mock(Recipe.class);
        Mockito.when(expectedResult.getName()).thenReturn("test");

        RecipeRepository recipeRepository = Mockito.mock(RecipeRepository.class);
        RecipeService recipeService = new RecipeService(recipeRepository);
        Mockito.when(recipeRepository.findByName("test")).thenReturn(Optional.empty());
        Mockito.when(recipeRepository.save(expectedResult)).thenReturn(expectedResult);

        //when
        Recipe result = recipeService.addRecipe(expectedResult);

        //then
        assertEquals(expectedResult, result);
    }

    @Test
    void given_repo_with_existing_recipe_when_add_new_recipe_with_not_unique_name_then_exception_should_be_thrown() {
        //given
        Recipe expectedResult = Mockito.mock(Recipe.class);
        Mockito.when(expectedResult.getName()).thenReturn("test");

        RecipeRepository recipeRepository = Mockito.mock(RecipeRepository.class);
        RecipeService recipeService = new RecipeService(recipeRepository);
        Mockito.when(recipeRepository.findByName("test")).thenReturn(Optional.of(expectedResult));

        //when
        //then
        assertThrows(RecipeAlreadyExistsException.class, ()->recipeService.addRecipe(expectedResult));
//        try {
//            recipeService.addRecipe(expectedResult);
//        }catch (RuntimeException e) {
//            assertTrue(e instanceof RecipeAlreadyExistsException);
//        }
    }
}