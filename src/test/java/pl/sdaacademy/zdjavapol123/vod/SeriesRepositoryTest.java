package pl.sdaacademy.zdjavapol123.vod;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest //stwórz testową bazę danych H2 na potrzeby testowe
class SeriesRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private SeriesRepository seriesRepository;

    @ParameterizedTest
    @ArgumentsSource(FindSeriesByIdArgumentsProvider.class)
    void given_input_elements_repo_when_access_series_by_id_arg_then_expectedResult_should_be_returned(
            List<Series> input,
            int arg,
            Optional<Series> expectedResult
    ) {
        //given
        input.forEach(item -> {
            testEntityManager.persist(item);
        });

        //when
        Optional<Series> result = seriesRepository.findById(arg);

        //then
        assertEquals(expectedResult, result);
    }

    @ParameterizedTest
    @ArgumentsSource(FindSeriesByEpisodesNumberArgumentsProvider.class)
    void given_input_elements_repo_when_access_series_by_number_of_episodes_arg_then_expectedResults_should_be_returned(
            List<Series> input,
            int numberOfEpisodes,
            List<Series> expectedResults
    ) {
        //given
        input.forEach(item -> {
            testEntityManager.persist(item);
        });

        //when
        List<Series> results = seriesRepository.findAllByNumberOfEpisodesGreaterThanEqual(numberOfEpisodes);

        //then
        assertEquals(expectedResults, results);
    }

    @ParameterizedTest
    @ArgumentsSource(FindSeriesBySeasonNumberArgumentsProvider.class)
    void given_input_elements_repo_when_access_series_by_season_number_arg_then_expectedResults_should_be_returned(
            List<Series> input,
            int seasonNumber,
            List<Series> expectedResults
    ) {
        //given
        input.forEach(item -> {
            testEntityManager.persist(item);
        });

        //when
        List<Series> results = seriesRepository.findAllBySeasonNumber(seasonNumber);

        //then
        assertEquals(expectedResults, results);
    }


    @ParameterizedTest
    @ArgumentsSource(FindSeriesByReleaseDateArgumentsProvider.class)
    void given_input_elements_repo_when_access_series_by_release_date_arg_then_expectedResults_should_be_returned(
            List<Series> input,
            LocalDate releaseDate,
            List<Series> expectedResults
    ) {
        //given
        input.forEach(item -> {
            testEntityManager.persist(item);
        });

        //when
        List<Series> results = seriesRepository.findAllByReleaseDateAfter(releaseDate);

        //then
        assertEquals(expectedResults, results);
    }
}