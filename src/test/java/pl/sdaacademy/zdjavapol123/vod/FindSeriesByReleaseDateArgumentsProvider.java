package pl.sdaacademy.zdjavapol123.vod;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class FindSeriesByReleaseDateArgumentsProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        List<Series> availableSeries = Arrays.asList(
                new Series(1, "Test1", "Test1", LocalDate.of(2020,2,2), 10, 1),
                new Series(2, "Test2", "Test2", LocalDate.of(2021,1, 1), 15, 2),
                new Series(3, "Test3", "Test3", LocalDate.of(2017, 12, 12), 2, 3),
                new Series(4, "Test4", "Test4", LocalDate.of(2022, 12, 24), 30, 2),
                new Series(5, "Test5", "Test5", LocalDate.of(2020, 2, 1), 4, 1)
        );
        return Stream.of(
                Arguments.of(
                        Collections.emptyList(),
                        LocalDate.of(2020, 1,1),
                        Collections.emptyList()
                ),
                Arguments.of(
                        availableSeries,
                        LocalDate.of(2020, 1,1),
                        Arrays.asList(
                                new Series(1, "Test1", "Test1", LocalDate.of(2020,2,2), 10, 1),
                                new Series(2, "Test2", "Test2", LocalDate.of(2021,1, 1), 15, 2),
                                new Series(4, "Test4", "Test4", LocalDate.of(2022, 12, 24), 30, 2),
                                new Series(5, "Test5", "Test5", LocalDate.of(2020, 2, 1), 4, 1)
                        )
                ),
                Arguments.of(
                        availableSeries,
                        LocalDate.of(2023,12,12),
                        Collections.emptyList()
                ),
                Arguments.of(
                        availableSeries,
                        LocalDate.of(2020,2,2),
                        Arrays.asList(
                                new Series(2, "Test2", "Test2", LocalDate.of(2021,1, 1), 15, 2),
                                new Series(4, "Test4", "Test4", LocalDate.of(2022, 12, 24), 30, 2)
                        )
                )
        );
    }
}
