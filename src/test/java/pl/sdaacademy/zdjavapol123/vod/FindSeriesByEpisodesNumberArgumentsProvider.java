package pl.sdaacademy.zdjavapol123.vod;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class FindSeriesByEpisodesNumberArgumentsProvider implements ArgumentsProvider {
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        List<Series> availableSeries = Arrays.asList(
                new Series(1, "Test1", "Test1", LocalDate.now(), 10, 1),
                new Series(2, "Test2", "Test2", LocalDate.now(), 15, 1),
                new Series(3, "Test3", "Test3", LocalDate.now(), 2, 1),
                new Series(4, "Test4", "Test4", LocalDate.now(), 30, 1),
                new Series(5, "Test5", "Test5", LocalDate.now(), 4, 1)
        );
        return Stream.of(
                Arguments.of(Collections.emptyList(), 10, Collections.emptyList()),
                Arguments.of(
                        availableSeries,
                        20,
                        Arrays.asList(new Series(4, "Test4", "Test4", LocalDate.now(), 30, 1))
                ),
                Arguments.of(
                        availableSeries,
                        40,
                        Collections.emptyList()
                ),
                Arguments.of(
                        availableSeries,
                        15,
                        Arrays.asList(
                                new Series(2, "Test2", "Test2", LocalDate.now(), 15, 1),
                                new Series(4, "Test4", "Test4", LocalDate.now(), 30, 1)
                        )
                ),
                Arguments.of(
                        availableSeries,
                        -1,
                        availableSeries
                )
        );
    }
}
