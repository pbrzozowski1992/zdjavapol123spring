package pl.sdaacademy.zdjavapol123;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

public class ParametrizedTestExample {


    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4,5})
    void test(int number) {
        System.out.println(number);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"test", "test2"})
    void test2(String item) {
        System.out.println(item);
    }
}
