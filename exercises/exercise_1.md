# Zadanie 1

Napisz REST API reprezentujące książkę kucharską. W ramach API zaimplementuj następujące zapytania:

Recipe
- name
- description
- duration (in min)
- numberOfPeople
- complexity (EASY, STANDARD, HARD)
- ingredients (na razie lista wszystkich składników oddzielona )->
ziemniaki,makaron,ser,wołowina

* pobieranie wszystkich przepisów
* pobieranie przepisu po id
* pobieranie wszystkich przepisów po składnikach
* pobieranie wszystkich przepisów po złożoności
* pobieranie wszystkich przepisów po czasie przygotowania
* dodawnie przepisów
* usuwanie przepisów
* aktualizację przepisów

Przepisy powinny być zapisywane do bazy danych.

## Część I

- struktura rozwiązania + podstawowy CRUD:
  - pobieranie wszystkich przepisów
  - dodawanie 
  - usuwanie

## Część II

- zaawansowane zapytania
  - pobieranie przepisu po id
  - pobieranie wszystkich przepisów po składnikach 
  - pobieranie wszystkich przepisów po złożoności 
  - pobieranie wszystkich przepisów po czasie przygotowania

# Zadanie 2

Zaimplementuj zaprezentowany mechanizm walidacji:

* zaproponuj rozwiązanie, które w ramach danego zapytania pozwoli paginować względem dowolnej liczby stron i rozmiaru (podobnie jak dla sortowanie)
* zaimplementuj mechanizm paginacji dla każdej z metod wyszukujących po zadanych parametrach

