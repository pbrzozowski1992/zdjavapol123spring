# Zadanie 1

Napisz metodę (SeriesRepository), która pobierze listę seriali mają więcej niż x episodów (greaterThan)
zaproponuj testy jednostkowe sprawdzające przypadek negatywny i pozytywny:

* sytuacja gdy znajduje się serial/seriale o zadanej ilości epizodów
* sytuacja gdy nie ma żadnego serialu spełniające zadane kryteria

https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation