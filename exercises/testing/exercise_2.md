#Zadanie 2

Napisz mechanizm dostarczający dane w ramach testów `findAllByNumberOfEpisodesGreaterThanEqual`

* napisz własną metodę dostarczającą dane w ramach poniżych scenariuszy:
- 0 elementów, szukamy: n, rezultat: brak elementu, -
- x elementów, szukamy: n (istnijacy w bazie), rezultat: elementy 
- x elementów, szukamy: n (niespełniający kryteriów), rezultat: brak
- x elementów, szukamy: n (wartość graniczna), rezultat: elementy
* napisz test sparametryzowany