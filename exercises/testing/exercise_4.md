# Zadanie 4

* W `SeriesRepository` dodaj motodę, która będzie pobierać seriale o konkretnym numerze sezonu
* Napisz testy dla nowo dodanej funkcjonalności biorąc pod uwagę wykorzystanie testów sparametryzowanych
* Zadabaj o przetestowanie następujących scenariuszu: brak danych na wejści, dany numer sezonu znajduję się wśród rekordów,
brak konkretnego sezonu wśród szukanych rekordów


## Dodatkowe

* W `SeriesRepository` dodaj motodę, która będzie pobierać seriale nowsze niż wskazana data
* Napisz testy dla nowo dodanej funkcjonalności biorąc pod uwagę wykorzystanie testów sparametryzowanych
* Zaproponuj zestawy danych pozwalających na przetestowanie kluczowych scenariuszy
