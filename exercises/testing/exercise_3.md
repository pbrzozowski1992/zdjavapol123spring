# Zadanie 3

Na podstawie zadania `exercise_2.md` zmodyfikuj istniejącą metodę dostarczającą dane i stwórz klasę implementującą
`ArgumentsProvider`. Użyj jej w celu sparametryzowania testu.