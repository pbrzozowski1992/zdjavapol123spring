# Zadanie 1

## Zadanie podstawowe

Napisz aplikację, która będzie realizować kolejno dwa
mechanizmy klonowania:
- shallow_copy
- deep_copy


Aplikacja zawiera następującą strukturę:
- class `Movie` o polach: tytuł, rok, wydania, Reżyser,
  gdzie reżyser jest osobnym obiektem realizowanym przez
  klasę:
- class `Director` o polach: imię, nazwisko, rodzaj
  kręconych filmów (enum, lub String).

Zaprezentuj na przykładzie każdy mechanizm klonowania.

## Zadanie dodatkowe

* W klasie Director zdefiniuj rodzaj filmu jako osobną klasę Genre zawierająca pole, nazwa i metadane. Zaprezentuj mechanizm deep copy w przypadku hierarchii trójpoziomowej
  Movie -> Director -> Genre

