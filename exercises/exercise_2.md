# Zadanie 2 - Obsługa wyjątków

## I

* zaimplementuj przedstawione na zajęciach rozwiązanie (integracj `NoRecipeFoundException`)
* (dodatkowe) zadbaj o to by każda metoda w której wyszukiwany jest element po id propagowała błąd `NoRecipeFoundException` w przypadku braku rekordu 

## II

* zaimplementuje mechanism serializacji błędów do json

## III

* zaimplementuj mechanizm do weryfikacji unikalności przepisu po nazwie. W systmie nie mogą istnieć dwa takie same przepisy o tej samej nazwie.
* (dodatkowo) zadbaj o weryfikację unikalności przepisu podczas aktualizacji