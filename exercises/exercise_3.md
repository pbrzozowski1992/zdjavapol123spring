# Zadanie 3 - Walidacja

## I

* zintegruj mechanizm walidacji zaprezentowany podczas zajęć
* dokonaj walidacji pozostałych pól klasy `Recipe`
  * description - nie może być pusty, 10-100 znaków
  * duration - nie może być mniejszcze od 0, nie może być nullem
  * numberOfPeople - nie może być mniejsze od 0, nie może być większe od 20
  * ingredients - opcjonalne
  * complexity - nie może być nullem