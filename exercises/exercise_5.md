# Zadanie 5:

## Zadanie podstawowe

Napisz mechanizm pobierania danych o planetach (name, surface_water, films) z świata star wars

https://swapi.dev/api/planets/1?format=json

## Zadanie dodatkowe

W ramach zadanie pobierz listę wszystkich dostępnych planet (użyj danych z paginacji)
  https://swapi.dev/api/planets?format=json
