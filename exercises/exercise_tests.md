# Zadania testowanie

Napisz testy dla poniższych metod w repozytorium:

```java
 List<Recipe> findAllByIngredientsContains(String ingredients, Pageable pageable);

    List<Recipe> findAllByComplexity(Complexity complexity, Pageable pageable);

    List<Recipe> findAllByDuration(int duration, Pageable pageable);

    Optional<Recipe> findByName(String name);//SELECT * FROM RECIPE WHERE name = 'name'
```

Uwzględnij scenariusz zarówno pozytwny jak i negatywny.
W każdym teście zastanów się co powinno być:
* na wejściu (given)
* co testujemy (when)
* jaki jest rezultat na wyjściu (then)